package io.Sh4dowCode.cc;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Utils {
	public static String pre ="�7[�cS�eCC�7] ";
    public static Player getPlayer(String name) {
        for(Player p : Bukkit.getOnlinePlayers()) {
            if(p.getName().equalsIgnoreCase(name))
                return p;
        }
        return null;
    }
    public static boolean isPlayer(String name) {
        for(Player p : Bukkit.getOnlinePlayers()) {
            if(p.getName().equalsIgnoreCase(name))
                return true;
        }
        return false;
    }
    
    public static void sendToPlayer(String msg, Player p) {
    	p.sendMessage(pre + msg);
    }
    public static void sendToAll(String msg) {
    	Bukkit.broadcastMessage(pre + msg);
    }
    public static void sendToCon(String msg) {
    	Bukkit.getConsoleSender().sendMessage(pre + msg);
    }
    public static void sendInval(Player p) {
    	sendToPlayer("�cInvalid Usage or Player\n  �6Usage:\n   �7- �e/cc �7| �6Clears the Chat of everyone online\n   �7- �e/cc �f<player> �7| �6Clears the Chat of the given player", p);
    }
}
