# SimpleChatClear #


## Download ##
You can get a pre-"compiled" Version of SCC on the Spigot Page.

https://www.spigotmc.org/resources/simple-chat-clear.44972/

## Implement SCC in your own Plugin ##
* Download the jar file from the Spigot page. You can also build it youself. (See "Build SCC yourself")
* Add the jar file to the Buildpath 
* Import `io.Sh4dowCode.cc.CC`. This file exposes to functions (voids), one for clearing the Chat for everyone online `cc()` and one for clearing just the Chat of one player. (`ccS()`)
    ```java
    io.Sh4dowCode.cc.CC.cc();
    //Needs no args, clears the chat of the whole Server
    io.Sh4dowCode.cc.CC.ccS(Player p);
    //Clears the Chat of P.
    ```
* Don't forget to include `SimpleChatClear` as Depedency in your plugin yml.
    ```yml
    depend:
    - SimpleChatClear
    ```

## Build SCC yourself ##
* Clone the repo into your Eclipse Workspace
* Create a new Project with the name SimpleChatClear (the name of the cloned Repo)
* Add spigot1.8.8.jar in the BuildPath
* Modify the code if you need to
* Right Click on the Project
    * Press Export
    * Select `Java/JAR file`
    * Select the following Files.
        * src/*
        * plugin.yml
    * Select the Path you want to export the file
    * Verify all other Settings
    * Press Export

## Contribution guidelines ##
If you want to contribute, please fork the Repo change whatever you think needs Changeing and create a pull request.

### Who do I talk to? ###
* https://bitbucket.org/sh4dowcode / https://bitbucket.org/sh4dowteam
* https://reddit.com/u/Sh4dowCode
* https://www.spigotmc.org/members/sh4dowcode.436648/
* https://github.com/sp1ritCS